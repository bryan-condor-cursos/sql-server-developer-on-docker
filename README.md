# Contenedor en Docker de una imagen de SQL Server Developer
Docker Compose configurado para levantar una image de SQLServer.

Esta configuraciones se basaron en documentación de Microsft.

- Instalación https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-ver15#ubuntu

- Configuración: https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-configure-docker?view=sql-server-ver15#persist

## Requisitos

- Docker: [Guía de Instalación](https://docs.docker.com/get-docker/)
- Docker Compose: [Guía de Instalación](https://docs.docker.com/compose/install/)

## Instrucciones
- Crear un carpeta en la ubicaciónde este archivo con el nombre `sqldata`
- Ejecutar el siguiente comando de docker-compose

``` bash
docker-compose up --detach
```


